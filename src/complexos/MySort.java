
package complexos;

/**
 *
 * @author amartins
 */
public class MySort {
    
    public static <T extends Comparable<T>> void sort(T[] vec){  
        boolean trocas = true;
        int n = vec.length;  
        
        while (trocas) {
            trocas = false;
            for(int i=0; i<n-1; i++) {
                if(vec[i].compareTo(vec[i+1]) > 0) {
                    T aux = vec[i];
                    vec[i] = vec[i+1];
                    vec[i+1] = aux;
                    trocas = true;
                }     
            }
        }
    }  
}
