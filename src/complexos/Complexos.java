package complexos;

import java.util.Comparator;

/**
 *
 * @author Angelo Martins
 */
public class Complexos implements Comparable<Complexos> {

    double a, b;
    private final double PrecComp = 0.005;

    public Complexos(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public Complexos() {
        this.a = 0;
        this.b = 0;
    }

    public Complexos(double m, float ang) {
        this.a = m * Math.cos(ang);
        this.b = m * Math.sin(ang);
    }

    public Complexos(String z) {
        String aux[] = new String[2];
        if (z.contains("+")) {
            aux = z.split("+");
        } else {
            if (z.contains("-")) {
                aux = z.split("-");
            }
        }
        a = Double.parseDouble(aux[0]);
        b = Double.parseDouble(aux[1]);
    }

    /**
     * Add two complex numbers
     *
     * @param z a complex number
     * @return a new complex number
     */
    public Complexos add(Complexos z) {
        Complexos c = new Complexos();
        c.a = z.a + this.a;
        c.b = z.b + this.b;
        return c;
    }

    /**
     * Subtract two complex numbers
     *
     * @param z a complex number
     * @return a new complex number
     */
    public Complexos subtract(Complexos z) {
        Complexos c = new Complexos();
        c.a = this.a - z.a;
        c.b = this.b - z.b;
        return c;
    }

    /**
     * Multiply two complex numbers
     *
     * @param z a complex number
     * @return a new complex number
     */
    public Complexos multiply(Complexos z) {
        Complexos c = new Complexos();
        c.a = z.a * this.a - z.b * this.b;
        c.b = z.a * this.b + z.b * this.a;
        return c;
    }

    /**
     * Divide two complex numbers
     *
     * @param z a complex number
     * @return a new complex number. If z is (0,0) returns (NaN, NaN).
     */
    public Complexos divide(Complexos z) {
        Complexos c = new Complexos();
        c.a = (z.a * this.a + z.b * this.b) / (z.a * z.a + z.b * z.b);
        c.b = (z.a * this.b - this.a * z.b) / (z.a * z.a + z.b * z.b);
        return c;
    }

    /**
     * Modulus of a complex number
     *
     * @return the absolute value as a double
     */
    public double absoluteValue() {
        return Math.sqrt(this.a * this.a + this.b * this.b);
    }

    /**
     * The angle of a complex number in polar coordinates
     *
     * @return the angle as double in the interval [-PI, PI]
     */
    public double angle() {
        double res;
        if (b == 0) {
            if (a < 0) {
                res = Math.PI;
            } else {
                res = 0;
            }
        } else if (this.a < 0) {
            res = Math.atan(Math.abs(this.b / this.a));
            if (b > 0) {
                res = Math.PI - res;
            } else {
                res = -(Math.PI - res);
            }
        } else if (a == 0) {
            if (b > 0) {
                res = Math.PI / 2;
            } else {
                res = -Math.PI / 2;
            }
        } else {
            res = Math.atan(this.b / this.a);
        }
        return res;
    }

    /**
     * The angle of a complex number in polar coordinates
     *
     * @return the angle as float in the interval [-180, 180]
     */
    public float angleDegrees() {
        return (float) (angle() / Math.PI * 180);
    }

    public Complexos conjugate() {
        Complexos c = new Complexos();
        c.a = this.a;
        c.b = -this.b;
        return c;
    }

    /**
     * Rotates a complex number by rot degrees
     *
     * @param rot angle in degrees
     */
    public void shiftDirectDegrees(float rot) {
        Complexos c = this.multiply(new Complexos(1d, (float) (rot / 180 * Math.PI)));
        this.a = c.a;
        this.b = c.b;
    }

    /**
     * Compares to complex numbers based on their modulus
     *
     * The resolution for two complexos being the same is defined by the class
     * variable PrecComp = 0,005d
     *
     * @param c
     * @return 1 if |this|>|c|, 0 if |this|==|c| and -1 otherwise
     */
    @Override
    public int compareTo(Complexos c) {
        double res = this.modulus() - c.modulus();

        if (res > this.PrecComp) {
            return 1;
        } else if (res < -this.PrecComp) {
            return -1;
        } else {
            return 0;
        }
    }
    /**
     *
     */
    public static Comparator<Complexos> ComplexosImagComparator = new Comparator<Complexos>() {
        @Override
        public int compare(Complexos c1, Complexos c2) {
            if (c1.b >= c2.b) {
                return 1;
            } else {
                return -1;
            }
        }
    };

    @Override
    public String toString() {
        return "(" + a + " + i" + b + ")";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.a) ^ (Double.doubleToLongBits(this.a) >>> 32));
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.b) ^ (Double.doubleToLongBits(this.b) >>> 32));
        return hash;
    }

    /**
     * Override of the equals() method
     *
     * @param other complex number
     * @return true if they are the same, false otherwise
     */
    @Override
    public boolean equals(Object other) {
        boolean result = false;
        if (other instanceof Complexos) {
            Complexos that = (Complexos) other;
            if (Double.isNaN(this.a) && Double.isNaN(that.a)
                    || Double.isNaN(this.b) && Double.isNaN(that.b)) {
                result = true;
            } else {
                result = (Math.abs(this.a - that.a) < this.PrecComp
                        && Math.abs(this.b - that.b) < this.PrecComp);
            }
        }
        return result;
    }
}
