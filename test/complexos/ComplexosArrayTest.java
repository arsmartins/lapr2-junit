/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package complexos;

import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Elsa Gomes
 */
public class ComplexosArrayTest {
    
    private ComplexosArray instance = new ComplexosArray();
    
    public ComplexosArrayTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
        Complexos c1 = new Complexos(1,2);
        instance.add(c1);
        c1 = new Complexos(2,2);
        instance.add(c1);
        c1 = new Complexos(0,-2);
        instance.add(c1);
        c1 = new Complexos(1,0);
        instance.add(c1);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class ComplexosArray.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        Complexos c = new Complexos(5,5);
        int expResult = instance.getSize()+1;
        instance.add(c);
        int result = instance.getSize();
        assertEquals(expResult, result);
        System.out.println("Array with new element: " + instance.toString());
    }
    
    /**
     * Test of add method, of class ComplexosArray.
     */
    @Test
    public void testAdd2() {
        System.out.println("add2 - compare the two arrays");
        String expResult = "[(-0.5 + i0.0)(1.0 + i0.0)(0.0 + i-2.0)(1.0 + i2.0)(2.0 + i2.0)]";
        Complexos c = new Complexos(-0.5,0);
        instance.add(c);
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of add method, of class ComplexosArray.
     */
    @Test
    public void testAddNull() {
        System.out.println("add null element");
        try {
            instance.add(null);
            fail("It can't add null");
        }
        catch (Exception e){
            System.out.println("OK! Null element exception raised");
        }
    }

    /**
     * Test of getElementI method, of class ComplexosArray.
     */
    @Test
    public void testGetElementI() {
        System.out.println("getElementI");
        int i = 3;
        Complexos expResult = new Complexos(2,2);
        Complexos result = instance.getElementI(i);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getElementI method, of class ComplexosArray.
     */
    @Test
    public void testGetElementIEmpty() {
        System.out.println("getElementI in position 0 of an empty array");
        int i = 0;
        ComplexosArray v = new ComplexosArray();
        Complexos result = v.getElementI(i);
        assertNull(result);
    }

    /**
     * Test of average method, of class ComplexosArray.
     */
    @Test
    public void testAverage() {
        System.out.println("average");
        double expResult = 2.016123776;
        double result = instance.average();
        assertEquals(expResult, result, 0.0005d);
    }
    
    /**
     * Test of average method, of class ComplexosArray.
     */
    @Test
    public void testAverageEmpty() {
        System.out.println("average of empty array");
        ComplexosArray v = new ComplexosArray();
        double result = v.average();
        assertEquals(result, Double.NaN, 0.0d);
    }

    /**
     * Test of getSize method, of class ComplexosArray.
     */
    @Test
    public void testGetSize() {
        System.out.println("getSize");
        int expResult = 4;
        int result = instance.getSize();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetSizeEmpty() {
        System.out.println("getSize of empty array");
        ComplexosArray v = new ComplexosArray();
        int expResult = 0;
        int result = v.getSize();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class ComplexosArray.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "[(1.0 + i0.0)(0.0 + i-2.0)(1.0 + i2.0)(2.0 + i2.0)]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
}
