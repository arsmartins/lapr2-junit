/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package complexos;

import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Angelo Martins <amm@isep.ipp.pt>
 */
public class ComplexosErradoTest {
    
    public ComplexosErradoTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAdd2() {
        System.out.println("add 2 numeros");
        ComplexosErrado z1 = new ComplexosErrado(1d,-1d);
        ComplexosErrado z2 = new ComplexosErrado(2d,1d);
        ComplexosErrado expResult = new ComplexosErrado(3d,0d);
        ComplexosErrado result = z1.add(z2);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testAdd3() {
        System.out.println("add, mas 3 numeros");
        ComplexosErrado z1 = new ComplexosErrado(1d,1d);
        ComplexosErrado z2 = new ComplexosErrado(1d,1d);
        ComplexosErrado z3 = new ComplexosErrado(1d,1d);
        ComplexosErrado expResult = new ComplexosErrado(3d,3d);
        ComplexosErrado result = z3.add(z1.add(z2));
        assertEquals(expResult, result);
    }
    
    /**
     * Test of multiply method, of class ComplexosErrado.
     */
    @Test
    public void testMultiply() {
        System.out.println("multiply");
        ComplexosErrado z = new ComplexosErrado(1d,-1d);
        ComplexosErrado instance = new ComplexosErrado(1d,1d);
        ComplexosErrado expResult = new ComplexosErrado(2d,0d);
        ComplexosErrado result = instance.multiply(z);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class ComplexosErrado.
     */
    @Test
    public void testToString() {
        double a = 1, b = 2;
        System.out.println("toString");
        ComplexosErrado instance = new ComplexosErrado(a,b);
        String expResult = "(" + a + " + i" +b + ")";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of add method, of class ComplexosErrado.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        ComplexosErrado z = new ComplexosErrado(1d,0d);
        ComplexosErrado instance = new ComplexosErrado(1d,1d);
        ComplexosErrado expResult = new ComplexosErrado(2d,1d);
        ComplexosErrado result = instance.add(z);
        assertEquals(expResult, result);
    }

    /**
     * Test of getModulo method, of class ComplexosErrado.
     */
    @Test
    public void testModulus() {
        System.out.println("getModulo");
        ComplexosErrado instance = new ComplexosErrado(3d,4d);
        double expResult = 5;
        double result = instance.modulus();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of compareTo method, of class ComplexosErrado.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        ComplexosErrado c = new ComplexosErrado(3d,4d);
        ComplexosErrado instance = new ComplexosErrado(4d,3d);
        int expResult = 0;
        int result = instance.compareTo(c);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class ComplexosErrado.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object other = new ComplexosErrado(-2, 5);
        ComplexosErrado instance = new ComplexosErrado(-2, 5);
        boolean expResult = true;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
    }

    @Test
    public void testEquals2() {
        System.out.println("equals");
        Object other = new ComplexosErrado(-2d, 5d);
        ComplexosErrado instance = new ComplexosErrado(2d, 5d);
        boolean expResult = false;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
    }

     /**
     * Test of equals method, of class ComplexosErrado.
     * 
     * Verifies if the equals method works with NaN.
     */
    @Test
    public void testEqualsNaN() {
        System.out.println("equals with NaN");
        Object other = new ComplexosErrado(Double.NaN, Double.NaN);
        ComplexosErrado instance = new ComplexosErrado(Double.NaN, Double.NaN);
        boolean expResult = true;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
    }
    /**
     * Test of subtract method, of class ComplexosErrado.
     */
    @Test
    public void testSubtract() {
        System.out.println("subtract");
        ComplexosErrado z = new ComplexosErrado(3d, 1d);
        ComplexosErrado instance = new ComplexosErrado(0d,1d);
        ComplexosErrado expResult = new ComplexosErrado(-3d, 0d);
        ComplexosErrado result = instance.subtract(z);
        assertEquals(expResult, result);
    }

    /**
     * Test of divide method, of class ComplexosErrado.
     */
    @Test
    public void testDivide() {
        System.out.println("divide");
        ComplexosErrado z = new ComplexosErrado (3d,0d);
        ComplexosErrado instance = new ComplexosErrado(0d,1d);
        ComplexosErrado expResult = new ComplexosErrado(0d,0.333d);
        ComplexosErrado result = instance.divide(z);
        assertEquals(expResult, result);
    }

     /**
     * Test of divide method, of class ComplexosErrado.
     */
    @Test
    public void testDivideZero() {
        System.out.println("divide");
        ComplexosErrado z = new ComplexosErrado (0d,0d);
        ComplexosErrado instance = new ComplexosErrado(3d,1d);
        ComplexosErrado expResult = new ComplexosErrado(Double.NaN, Double.NaN);
        ComplexosErrado result = instance.divide(z);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of angle method, of class ComplexosErrado.
     */
    @Test
    public void testAngle() {
        System.out.println("angle");
        ComplexosErrado instance = new ComplexosErrado(1.73205080757d,1d);
        double expResult = Math.PI/6;
        double result = instance.angle();
        assertEquals(expResult, result, 0.0005d);
    }
    
    /**
     * Test of angle method, of class ComplexosErrado.
     */
    @Test
    public void testAngle2Q() {
        System.out.println("angle");
        ComplexosErrado instance = new ComplexosErrado(-2d,2d);
        double expResult = 3*Math.PI/4;
        double result = instance.angle();
        assertEquals(expResult, result, 0.0005d);
    }

     /**
     * Test of angle method, of class ComplexosErrado.
     */
    @Test
    public void testAngle3Q() {
        System.out.println("angle");
        ComplexosErrado instance = new ComplexosErrado(-2d,-2d);
        double expResult = -3*Math.PI/4;
        double result = instance.angle();
        assertEquals(expResult, result, 0.0005d);
    }
    
    /**
     * Test of angle method, of class ComplexosErrado.
     */
    @Test
    public void testAngle4Q() {
        System.out.println("angle");
        ComplexosErrado instance = new ComplexosErrado(2d,-2d);
        double expResult = -Math.PI/4;
        double result = instance.angle();
        assertEquals(expResult, result, 0.0005d);
    }
    
     /**
     * Test of angle method, of class ComplexosErrado.
     */
    @Test
    public void testAngle90() {
        System.out.println("angle");
        ComplexosErrado instance = new ComplexosErrado(0d,2d);
        double expResult = Math.PI/2;
        double result = instance.angle();
        assertEquals(expResult, result, 0.0005d);
    }
    
    /**
     * Test of angleDegrees method, of class ComplexosErrado.
     */
    @Test
    public void testAngleDegrees() {
        System.out.println("angleDegrees");
        ComplexosErrado instance = new ComplexosErrado(-2d,2d);
        float expResult = 135f;
        float result = instance.angleDegrees();
        assertEquals(expResult, result, 0.0005d);
    }
    
    /**
     * Test of angleDegrees method, of class ComplexosErrado.
     */
    @Test
    public void testAngleDegrees3Q() {
        System.out.println("angleDegrees");
        ComplexosErrado instance = new ComplexosErrado(-2d,-2d);
        float expResult = -135f;
        float result = instance.angleDegrees();
        assertEquals(expResult, result, 0.0005d);
    }

    /**
     * Test of conjugate method, of class ComplexosErrado.
     */
    @Test
    public void testConjugate() {
        System.out.println("conjugate");
        ComplexosErrado instance = new ComplexosErrado(2d, 3d);
        ComplexosErrado expResult = new ComplexosErrado (2d, -3d);
        ComplexosErrado result = instance.conjugate();
        assertEquals(expResult, result);
    }

    /**
     * Test of shiftDirectDegrees method, of class ComplexosErrado.
     */
    @Test
    public void testShiftDirectDegrees() {
        System.out.println("shiftDirectDegrees");
        float rot = 90F;
        ComplexosErrado instance = new ComplexosErrado(1d, 1d);
        instance.shiftDirectDegrees(rot);
        ComplexosErrado expResult = new ComplexosErrado (-1d, 1d);
        assertEquals(expResult, instance);
    }
    /**
     * Test of shiftDirectDegrees method, of class ComplexosErrado.
     */
    @Test
    public void testShiftDirectDegreesNeg() {
        System.out.println("shiftDirectDegrees");
        float rot = -180F;
        ComplexosErrado instance = new ComplexosErrado(1d, -1d);
        instance.shiftDirectDegrees(rot);
        ComplexosErrado expResult = new ComplexosErrado (-1d, 1d);
        assertEquals(expResult, instance);
    }
}
