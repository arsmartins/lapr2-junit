/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package complexos;

import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Angelo Martins <amm@isep.ipp.pt>
 */
public class ComplexosTest {
    
    public ComplexosTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAdd2() {
        System.out.println("add 2 numeros");
        Complexos z1 = new Complexos(1d,-1d);
        Complexos z2 = new Complexos(2d,1d);
        Complexos expResult = new Complexos(3d,0d);
        Complexos result = z1.add(z2);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testAdd3() {
        System.out.println("add, mas 3 numeros");
        Complexos z1 = new Complexos(1d,1d);
        Complexos z2 = new Complexos(1d,1d);
        Complexos z3 = new Complexos(1d,1d);
        Complexos expResult = new Complexos(3d,3d);
        Complexos result = z3.add(z1.add(z2));
        assertEquals(expResult, result);
    }
    
    /**
     * Test of multiply method, of class Complexos.
     */
    @Test
    public void testMultiply() {
        System.out.println("multiply");
        Complexos z = new Complexos(1d,-1d);
        Complexos instance = new Complexos(1d,1d);
        Complexos expResult = new Complexos(2d,0d);
        Complexos result = instance.multiply(z);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Complexos.
     */
    @Test
    public void testToString() {
        double a = 1, b = 2;
        System.out.println("toString");
        Complexos instance = new Complexos(a,b);
        String expResult = "(" + a + " + i" +b + ")";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of add method, of class Complexos.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        Complexos z = new Complexos(1d,0d);
        Complexos instance = new Complexos(1d,1d);
        Complexos expResult = new Complexos(2d,1d);
        Complexos result = instance.add(z);
        assertEquals(expResult, result);
    }

    /**
     * Test of getModulo method, of class Complexos.
     */
    @Test
    public void testabsoluteValue() {
        System.out.println("getModulo");
        Complexos instance = new Complexos(3d,4d);
        double expResult = 5;
        double result = instance.absoluteValue();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of compareTo method, of class Complexos.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        Complexos c = new Complexos(3d,4d);
        Complexos instance = new Complexos(4d,3d);
        int expResult = 0;
        int result = instance.compareTo(c);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Complexos.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object other = new Complexos(-2, 5);
        Complexos instance = new Complexos(-2, 5);
        boolean expResult = true;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
    }

    @Test
    public void testEquals2() {
        System.out.println("equals");
        Object other = new Complexos(-2d, 5d);
        Complexos instance = new Complexos(2d, 5d);
        boolean expResult = false;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
    }

     /**
     * Test of equals method, of class Complexos.
     * 
     * Verifies if the equals method works with NaN.
     */
    @Test
    public void testEqualsNaN() {
        System.out.println("equals with NaN");
        Object other = new Complexos(Double.NaN, Double.NaN);
        Complexos instance = new Complexos(Double.NaN, Double.NaN);
        boolean expResult = true;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
    }
    /**
     * Test of subtract method, of class Complexos.
     */
    @Test
    public void testSubtract() {
        System.out.println("subtract");
        Complexos z = new Complexos(3d, 1d);
        Complexos instance = new Complexos(0d,1d);
        Complexos expResult = new Complexos(-3d, 0d);
        Complexos result = instance.subtract(z);
        assertEquals(expResult, result);
    }

    /**
     * Test of divide method, of class Complexos.
     */
    @Test
    public void testDivide() {
        System.out.println("divide");
        Complexos z = new Complexos (3d,0d);
        Complexos instance = new Complexos(0d,1d);
        Complexos expResult = new Complexos(0d,0.333d);
        Complexos result = instance.divide(z);
        assertEquals(expResult, result);
    }

     /**
     * Test of divide method, of class Complexos.
     */
    @Test
    public void testDivideZero() {
        System.out.println("divide");
        Complexos z = new Complexos (0d,0d);
        Complexos instance = new Complexos(3d,1d);
        Complexos expResult = new Complexos(Double.NaN, Double.NaN);
        Complexos result = instance.divide(z);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of angle method, of class Complexos.
     */
    @Test
    public void testAngle() {
        System.out.println("angle");
        Complexos instance = new Complexos(1.73405d,1d);
        double expResult = Math.PI/6;
        double result = instance.angle();
        assertEquals(expResult, result, 0.0005d);
    }
    
    /**
     * Test of angle method, of class Complexos.
     */
    @Test
    public void testAngle2Q() {
        System.out.println("angle");
        Complexos instance = new Complexos(-2d,2d);
        double expResult = 3*Math.PI/4;
        double result = instance.angle();
        assertEquals(expResult, result, 0.0005d);
    }

     /**
     * Test of angle method, of class Complexos.
     */
    @Test
    public void testAngle3Q() {
        System.out.println("angle");
        Complexos instance = new Complexos(-2d,-2d);
        double expResult = -3*Math.PI/4;
        double result = instance.angle();
        assertEquals(expResult, result, 0.0005d);
    }
    
    /**
     * Test of angle method, of class Complexos.
     */
    @Test
    public void testAngle4Q() {
        System.out.println("angle");
        Complexos instance = new Complexos(2d,-2d);
        double expResult = -Math.PI/4;
        double result = instance.angle();
        assertEquals(expResult, result, 0.0005d);
    }
    
     /**
     * Test of angle method, of class Complexos.
     */
    @Test
    public void testAngle90() {
        System.out.println("angle");
        Complexos instance = new Complexos(0d,2d);
        double expResult = Math.PI/2;
        double result = instance.angle();
        assertEquals(expResult, result, 0.0005d);
    }
    
    /**
     * Test of angleDegrees method, of class Complexos.
     */
    @Test
    public void testAngleDegrees() {
        System.out.println("angleDegrees");
        Complexos instance = new Complexos(-2d,2d);
        float expResult = 135f;
        float result = instance.angleDegrees();
        assertEquals(expResult, result, 0.0005d);
    }
    
    /**
     * Test of angleDegrees method, of class Complexos.
     */
    @Test
    public void testAngleDegrees3Q() {
        System.out.println("angleDegrees");
        Complexos instance = new Complexos(-2d,-2d);
        float expResult = -135f;
        float result = instance.angleDegrees();
        assertEquals(expResult, result, 0.0005d);
    }

    /**
     * Test of conjugate method, of class Complexos.
     */
    @Test
    public void testConjugate() {
        System.out.println("conjugate");
        Complexos instance = new Complexos(2d, 3d);
        Complexos expResult = new Complexos (2d, -3d);
        Complexos result = instance.conjugate();
        assertEquals(expResult, result);
    }

    /**
     * Test of shiftDirectDegrees method, of class Complexos.
     */
    @Test
    public void testShiftDirectDegrees() {
        System.out.println("shiftDirectDegrees");
        float rot = 270F;
        Complexos instance = new Complexos(1d, 1d);
        instance.shiftDirectDegrees(rot);
        Complexos expResult = new Complexos (1d, -1d);
        assertEquals(expResult, instance);
    }
    
    /**
     * Test of shiftDirectDegrees method, of class Complexos.
     */
    @Test
    public void testShiftDirectDegreesNeg() {
        System.out.println("shiftDirectDegrees");
        float rot = -180F;
        Complexos instance = new Complexos(1d, -1d);
        instance.shiftDirectDegrees(rot);
        Complexos expResult = new Complexos (-1d, 1d);
        assertEquals(expResult, instance);
    }
}
